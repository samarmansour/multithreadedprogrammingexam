﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadedExam
{
    public class Workers
    {
        public List<Task> TaskList(object list)
        {
            List<Task> t = (List<Task>)list;
            int taskNumber = 5;
            for (int i = 0; i < taskNumber; i++)
            {
                t.Add(Task.Run(() =>
                {
                    Thread.Sleep(1000);
                    return i;
                }));
            }
            return t;
        }
        
        
    }
}
