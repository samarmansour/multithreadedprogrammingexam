﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadedExam.Restaurant_ques7
{
    public class Kichen
    {
        public event EventHandler<DishEventArgs> DishReady;

        public void OnDishReady()
        {
            if (DishReady != null)
            {
                DishReady.Invoke(this, new DishEventArgs());
            }
        }
        public void PrepareDish()
        {
            Console.WriteLine("Preparing dish.....");
            OnDishReady();
        }
    }
}
