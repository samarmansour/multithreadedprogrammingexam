﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadedExam.Restaurant_ques7
{
    public class DishEventArgs
    {
        public string DishName { get; set; }
    }
}
