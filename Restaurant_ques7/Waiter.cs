﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadedExam.Restaurant_ques7
{
    public class Waiter
    {
        public void OnDishReadyEventHandler(object sender, DishEventArgs dishEventArgs)
        {
            Console.WriteLine($"Serving the {dishEventArgs.DishName} dish to the customers");
        }
    }
}
