﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadedExam
{
    public class DanceClub
    {
        public ManualResetEvent host = new ManualResetEvent(false);
        public AutoResetEvent autoHost = new AutoResetEvent(false);

        public void EnterClub()
        {
            Console.WriteLine("Waiting to enter...");
            host.WaitOne();
            Console.WriteLine("next host can enter");
            host.Set();
        }

        public void AutoEnterClub()
        {
            Console.WriteLine("Waiting to enter...");
            autoHost.WaitOne();
            Console.WriteLine("next host can enter");
        }
    }
}
