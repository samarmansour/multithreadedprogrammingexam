﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadedExam
{
    class Questions
    {
        //ques 1 - random numbers LINQ
        public  void RandomNumbersLinq()
        {
            List<int> numbers = new List<int>();
            Random rnd = new Random();
            int max = 100;

            for (int i = 0; i < max; i++)
            {
                numbers.Add(rnd.Next(0,51));
            }

            //A
            Console.WriteLine("=================={A}======================");
            var smallerThan10 = numbers.Where(c => c < 10);
            Console.WriteLine("smaller numbers than 10:");
            smallerThan10.ToList().ForEach(x => Console.Write(x + " "));
            Console.WriteLine();

            //B
            Console.WriteLine("=================={B}======================");
            var dividedBy3 = numbers.Where(c => c % 3 == 0);
            Console.WriteLine("Divided by 3:");
            dividedBy3.ToList().ForEach(x => Console.Write(x + " "));
            Console.WriteLine();

            //C
            Console.WriteLine("=================={C}======================");
            var biggerThan20_EvenNumbers = numbers.Where(c => c < 20 && c % 2 == 0);
            Console.WriteLine("Bigger than 20 and it's even numbers:");
            biggerThan20_EvenNumbers.ToList().ForEach(x => Console.Write(x + " "));
            Console.WriteLine();

            //D
            Console.WriteLine("=================={D}======================");
            var sortedList = numbers.OrderByDescending(c => c);
            Console.WriteLine($"Sorted list:");
            sortedList.ToList().ForEach(x => Console.Write(x + " "));
            Console.WriteLine();

        }

        //ques 2 - names LINQ
        public void StringListLinq()
        {
            List<string> names = new List<string>() { "Sam", "Jack", "Saleem", "Laura", "Omar", "Tim", "Pablo", "Camilo", "Rayan", "Samia" };

            //A
            Console.WriteLine("=================={A}======================");
            var namebiggerthan4 = names.Where(c => c.Length > 4);
            Console.WriteLine("Name length bigger than 4");
            namebiggerthan4.ToList().ForEach(x => Console.Write(x + " "));
            Console.WriteLine();

            //B
            Console.WriteLine("=================={B}======================");
            var containsA = names.Where(c => c.ToUpper().Contains('A'));
            Console.WriteLine("Names contains letter A");
            containsA.ToList().ForEach(x => Console.Write(x + " "));
            Console.WriteLine();

            //C
            Console.WriteLine("=================={C}======================");
            var sortedfromABC = names.OrderBy(c => c).ToList();
            Console.WriteLine("Sorted by ABC");
            sortedfromABC.ToList().ForEach(x => Console.Write(x + " "));
            Console.WriteLine();
        }

        //ques 3 - delegates
        public delegate int SumNumbers(int n1, int n2);
        public int Add(SumNumbers sum, int num1, int num2)
        {
            int result = sum(num1, num2);
            return result;
        }


        // ques 9 - run time
        //A
        public void LongOperation()
        {
            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < 1000000000; i++)
            {
                sw.Start();
            }
            sw.Stop();
            Console.WriteLine("Done");
            Console.WriteLine($"Time elapsed, without threads: {sw.Elapsed}");
            
        }
        //B
        public void LongOperationB()
        {
            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < 1000000000; i++)
            {
                sw.Start();
            }
            sw.Stop();
            Console.WriteLine("Done");
            Console.WriteLine($"Time elapsed, with threads: {sw.Elapsed}");

        }

        //ques 14
        public void DownloadFile()
        {
            Console.WriteLine("Downloading file....");
            Thread.Sleep(10000);
            Console.WriteLine("Completed");
        }
        public int Calculate(int num1, int num2)
        {
            return num1 * num2;
        }

        //ques 38
        public async Task ReadFileAsync(string dir, string file)
        {
            List<string> fileText = new List<string>();
            Console.WriteLine("Async Read File has started!");
            using (StreamReader outputFile = new StreamReader(Path.Combine(dir,file)))
            {
                string line;
                while((line = await outputFile.ReadLineAsync()) != null)
                {
                    fileText.Add(line);
                }
            }
            Console.WriteLine("Async reader file has completed");
        }

    }
}
