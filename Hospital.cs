﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadedExam
{
    public class Hospital
    {
        private static object key1 = new object();
        private static object key2 = new object();

        public void DoctorTreatment()
        {
            try
            {
                Monitor.Enter(key1);
                Console.WriteLine("Waiting for my turn");
                Monitor.Pulse(key1);
            }
            finally
            {
                Monitor.Exit(key1);
            }     

        }

        public void NurseCheck()
        {
            try
            {
                Monitor.Enter(key2);
                Console.WriteLine("Nurse is checking");
                Monitor.Wait(500);
                Monitor.Pulse(key2);
                Console.WriteLine("Next patient please!");
            }
            finally
            {
                Monitor.Exit(key2);
            }
        }
    }
}
