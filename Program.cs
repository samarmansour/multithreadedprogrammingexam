﻿using MultiThreadedExam.Restaurant_ques7;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadedExam
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Questions q = new Questions();
            //1
            q.RandomNumbersLinq();
            Console.WriteLine();

            //2
            q.StringListLinq();
            Console.WriteLine();

            //3
            Console.WriteLine(q.Add((num1, num2) => num1 + num2, 1, 2));
            Console.WriteLine();

            //restaurant 7
            Kichen k = new Kichen();
            Waiter w = new Waiter();
            k.DishReady += w.OnDishReadyEventHandler;
            k.PrepareDish();

            // ques 9 - running time
            int num = 5;
            for (int i = 0; i < num; i++)
            {
                q.LongOperation();
            }

            for (int i = 0; i < num; i++)
            {
                Thread thread = new Thread(q.LongOperationB);
                thread.Start();
            }

            //ques 12 - threads + lambda
            //A
            Thread t = new Thread(() => Console.WriteLine("Hello world"));
            t.Start();

            //B
            List<int> numbers = new List<int>() {10,5,3,0,95,8,4,7,6,20,3,4,1,55,5 };
            Thread t2 = new Thread((x) => Console.WriteLine( numbers.Find(i => (i == 5))));
            t2.Start("searching....");

            //ques 14
            //A - thread
            Thread tDownload = new Thread(q.DownloadFile);
            tDownload.Start();

            //B - threadpool
            Task task = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(10);
                Console.WriteLine(q.Calculate(5,4));
            });

            //ques 19 - Hospital
            Hospital hospital = new Hospital();
            for (int i = 0; i < 5; i++)
            {
                hospital.DoctorTreatment();
                Thread.Sleep(10000);
                hospital.NurseCheck();
            }

            //ques 22 - dance club
            //A - Manual reset event
            DanceClub danceClub = new DanceClub();
            for (int i = 0; i < 10; i++)
            {
                Thread threadDance = new Thread(danceClub.EnterClub);
                threadDance.Start();
                Thread.Sleep(100);
                danceClub.host.Set();
            }

            //B - Auto reset event
            DanceClub danceClubAuto = new DanceClub();
            for (int i = 0; i < 10; i++)
            {
                Thread auto = new Thread(danceClub.EnterClub);
                auto.Start();
                Thread.Sleep(200);
            }

            //ques 25 B - Task factory
            Task task1 = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(10);
            }, TaskCreationOptions.LongRunning);
            task1.Start();

            //ques 26 - Task sum numbers
            int n1 = 5;
            int n2 = 20;
            Task<int> res = Task.Run<int>(() =>
            {
                Thread.Sleep(10);
                return n1 + n2;
            });
            Console.WriteLine(res.Result);

            //ques 27 B - continueWith
            Task chainTasks = Task.Run(() =>
            {
                Thread.Sleep(10);
                Console.WriteLine("Welcome");
            }).ContinueWith((Task antecendent) =>
            {
                Console.WriteLine("Goodbye");
            });
            Console.WriteLine(chainTasks);

            // ques 27 - C
            Task failedchainTasks = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(10);
                Console.WriteLine("Welcome");
            }).ContinueWith((Task antecendent) =>
            {
                Console.WriteLine("Goodbye");
            }, TaskContinuationOptions.OnlyOnFaulted);
            Console.WriteLine(failedchainTasks);

            //ques 28 - task not failed
            Task taskNotFailed = Task.Run(() =>
            {
                Console.WriteLine(Task.CurrentId);
            }).ContinueWith((Task papa) => 
            {
                Console.WriteLine("Continue after failure");
                Console.WriteLine(papa.Status);
                Console.WriteLine(papa.IsFaulted);
            });

            // ques 29 - B
            List<Task> tasksLists = new List<Task>();
            for (int i = 0; i < 5; i++)
            {
                Task task2 = Task.Run(() =>
                {
                    Thread.Sleep(500);
                });
                tasksLists.Add(task2);
            }
            int finished = 0;
            while (finished != tasksLists.Count)
            {
                finished = 0;
                foreach (var item in tasksLists)
                {
                    finished += item.Status == TaskStatus.RanToCompletion ? 1 : 0;
                }
                Thread.Sleep(10);
            }
            Console.WriteLine("All tasks are done");


            // ques 30 - B
            Task firstTask = Task.Run(() =>
            {
                Thread.Sleep(rnd.Next(500, 1000));

            }).ContinueWith((Task antecendent) =>
            {
                Thread.Sleep(rnd.Next(500, 1000));
                if (antecendent.Status == TaskStatus.RanToCompletion)
                    Console.WriteLine("second task is done");
            }).ContinueWith((Task antecendent) =>
            {
                Thread.Sleep(rnd.Next(500, 1000));
                if (antecendent.Status == TaskStatus.RanToCompletion)
                    Console.WriteLine("third task is done");

            });

            // ques 31 
            Task task_failure = Task.Run(() =>
            {
                Console.WriteLine("divide by zero task");
                int a = 5;
                int b = 0;
                int c = a / b;
                Console.WriteLine("code will not be reached. task faulted.");
            }).ContinueWith((Task papa) =>
            {
                Console.WriteLine("continue with after failure");
                Console.WriteLine(papa.Status);
                Console.WriteLine(papa.IsFaulted);
            });

            //ques 31 - B
            Task<int> task_failure2 = Task.Run<int>(() =>
            {
                Console.WriteLine("divide by zero task");
                int a = 5;
                int b = 0;
                return a / b; // wil crash the program
            });
            task_failure2.Wait();

            //ques 31 - c
            Task.Run(() =>
            {
                int a = 5;
                int b = 0;
                int c = a / b;
            }).ContinueWith((Task antecendent) =>
            {
                Console.WriteLine("is faulted?");
                Console.WriteLine(antecendent.IsFaulted);
                antecendent.Exception.Handle(e =>
                {
                    Console.WriteLine("Handler............");
                    Console.WriteLine(e);
                    return true;
                });
            }, TaskContinuationOptions.OnlyOnFaulted);
            Thread.Sleep(1000);

            // ques 33 
            Console.WriteLine("Timer is starting");
            Task t1 = Task.Run(MyTimer);

            Console.WriteLine("Press [Enter] to stop ...");
            Console.ReadLine();
            source.Cancel();
            Console.WriteLine("Timer is stopped: X");


            // ques 34
            List<int> queue = new List<int>();
            Workers workers = new Workers();
            for (int i = 0; i < 5; i++)
            {
                queue.Add(Task.Run<int>(() =>
                {
                   workers.TaskList(new int[] { i * 2 });
                }));
            }


            //ques 38 - async await
            var file = q.ReadFileAsync(@"C:\mydir", @"C:\mydir\myfile.text");
            Console.WriteLine(file);

        }

        // ques 33
        static CancellationTokenSource source = new CancellationTokenSource();
        static void MyTimer()
        {
            int count = 0;
            while (true)
            {
                Thread.Sleep(1000);
                count++;
            }
            if(source.IsCancellationRequested)
            {
                throw new OperationCanceledException();
            }

        }

    }
}
